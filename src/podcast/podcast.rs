use rss::*;
use crate::result::*;
use crate::config::Config;

pub struct Podcast {
    pub config: Config,
    pub channel: Channel,
    pub url: String,
}

impl Podcast {
    pub fn new(url: &str) -> Result<Self> {
        Ok(Podcast {
            config: Config{setting: "hw".into()},
            channel: Channel::from_url(&url[..])?,
            url: url.into(),
        })
    }
    pub fn items(&self) -> &[Item] {
        self.channel.items()
    }
}
