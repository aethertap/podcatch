use rss::Error as RssError;
use std::error::Error as StdError;
use std::convert::From;
use std::fmt;
use std::sync::PoisonError;

//#[allow(unused)]
#[derive(Debug)]
pub enum Error {
    Rss(RssError),
    Other(String),
}

impl StdError for Error {
    fn description(&self) -> &str {
        use self::Error::*;
        match *self {
            Rss(ref e) => e.description(),
            Other(ref s) => &s[..],
        }
    }
    fn cause(&self) -> Option<&StdError> {
        use self::Error::*;
        match *self {
            Rss(ref e) => Some(e),
            Other(_) => None,
        }
    }
}

impl fmt::Display for Error {
    fn fmt(&self, out:&mut fmt::Formatter) -> fmt::Result {
        use self::Error::*;
        match *self {
            Rss(ref e) => write!(out,"RSS Error: {}",e),
            Other(ref s) => write!(out,"Dependency Error: {}",s),
        }
    }
}

impl From<RssError> for Error {
    fn from(other: RssError) -> Self {
        Error::Rss(other)
    }
}

impl<T> From<PoisonError<T>> for Error {
    fn from(other: PoisonError<T>) -> Self {
        Error::Other(format!("{}",other))
    }
}
//impl<T:StdError> From<T> for Error {
    //fn from(other: T) -> Self {
        //Error::Other(format!("{}",other))
    //}
//}
