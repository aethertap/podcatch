
mod error;
pub use self::error::*;

pub type Result<T> = std::result::Result<T,Error>;
