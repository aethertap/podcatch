extern crate rss;
#[macro_use] extern crate lazy_static;

mod result;
mod app;
mod config;
mod podcast;

use self::podcast::*;

fn main() {
    let p = Podcast::new("http://feeds.feedburner.com/artofmanlinesspodcast").unwrap();
    let conf = app::with_config(|conf| conf.setting.clone()).unwrap();
    println!("config setting: {}",conf);
    app::with_config_mut(|conf| conf.setting = String::from("updated"))
        .expect("Failed to update config setting!");
    println!("config setting: {}",app::with_config(|conf| conf.setting.clone()).unwrap());
    for item in &p.items()[0..10] {
        println!("Title: {}", item.title().unwrap_or("<Untitled>"));
    }
}
