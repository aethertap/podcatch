use crate::config::*;
use std::sync::RwLock;
use crate::result::*;

lazy_static!{
    static ref CONFIG: RwLock<Config> = RwLock::new(Config::new());
}

pub fn with_config<T,F:FnOnce(&Config)->T>(f:F) -> Result<T> {
    Ok(CONFIG.read().map(|conf| f(&*conf))?)
}

pub fn with_config_mut<T,F:FnOnce(&mut Config)->T>(f:F) -> Result<T> {
    Ok(CONFIG.write().map(|mut conf| f(&mut *conf))?)
}
